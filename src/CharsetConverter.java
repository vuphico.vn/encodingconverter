import com.ibm.icu.text.CharsetDetector;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 * This will convert charset of all files under /testdata from a charset to other charset.
 * <p></p>
 * Usage instruction:
 * <ul>
 *    <li>Move files which you wanna convert charset to /testdata folder.</li>
 *    <li>(Optional) Change value of 2 fields: fromCharset and DST_CHARSET in the source code as your specific.</li>
 *    <li>Start program to convert all files under /testdata</li>
 * </ul>
 */
public class CharsetConverter {
   // Charset you think it is charset of your files, set NULL to tell program auto-detect.
   private Charset fromCharset = Charset.forName("SHIFT_JIS");
   // Charset which program will convert to, free to change it but cannot be null.
   private final Charset TO_CHARSET = StandardCharsets.UTF_8;

   private int skipSinceSameCharsetFileCount;
   private int skipSinceCouldNotDetectFileCount;

   private CharsetConverter() {
      char fs = File.separatorChar;
      String root = new File("").getAbsolutePath();
      String testDataPath = String.format("%s%c%s", root, fs, "testdata");

      System.out.println("TestDataFilePath: " + testDataPath);

      File inFile = new File(testDataPath);

      scanAndConvertEncoding(inFile);

      System.out.println();

      if (skipSinceSameCharsetFileCount > 0) {
         System.out.println(String.format("Skip convert for %d files since same charset",
            skipSinceSameCharsetFileCount));
      }
      if (skipSinceCouldNotDetectFileCount > 0) {
         System.err.println(String.format("Skip convert for %d files since could not detect charset",
            skipSinceCouldNotDetectFileCount));
      }
   }

   /**
    * Detect charset and convert content of this file. Note that, this is approximate action, not 100% reliable.
    */
   private void scanAndConvertEncoding(File file) {
      if (file.isFile()) {
         Charset detectedCharset = detectCharset(file);

         if (detectedCharset == null) {
            ++skipSinceCouldNotDetectFileCount;
            System.err.println(String.format("Skipped convert since could not detect charset for file %s",
               file.getPath()));
         }
         else {
            if (TO_CHARSET.equals(detectedCharset)) {
               ++skipSinceSameCharsetFileCount;
               System.out.println(String.format("Skipped convert since same charset to convert for file %s",
                  file.getName()));
            }
            else {
               Charset fromCharset = (this.fromCharset == null) ? detectedCharset : this.fromCharset;
               convertEncoding(file, fromCharset, TO_CHARSET);
            }
         }
      }
      else if (file.isDirectory()) {
         File[] fs = file.listFiles();

         if (fs != null) {
            for (File f : fs) {
               scanAndConvertEncoding(f);
            }
         }
      }
   }

   private void convertEncoding(File file, Charset fromCharset, Charset toCharset) {
      try {
         FileInputStream fis = new FileInputStream(file);
         byte[] content = fis.readAllBytes();
         fis.close();

         String input = new String(content, fromCharset);

         FileOutputStream fos = new FileOutputStream(file);
         fos.write(input.getBytes(toCharset));
         fos.close();

         System.out.println(String.format("Converted charset %s -> %s for file %s",
            fromCharset.name(), toCharset.name(), file.getPath()));
      }
      catch (Exception e) {
         e.printStackTrace();
      }
   }

   private Charset detectCharset(File file) {
      try {
         BufferedInputStream is = new BufferedInputStream(new FileInputStream(file));

         BufferedInputStream bis = new BufferedInputStream(is);
         CharsetDetector charsetDetector = new CharsetDetector();
         charsetDetector.setText(bis);
         String charsetName = charsetDetector.detect().getName();

         return charsetName == null ? null : Charset.forName(charsetName);
      }
      catch (Exception e) {
         e.printStackTrace();
         return null;
      }
   }

   public static void main(String[] args) {
      new CharsetConverter();
   }
}
