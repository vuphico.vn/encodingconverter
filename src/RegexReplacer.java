import java.io.*;

/**
 * This will scan all files, then for each file, it replaces all substring
 * which matches with given regular-expression to a given replacement.
 */
public class RegexReplacer {
   private static final String REGEX = "charset=[\\s\\S]*\"";
   private static final String REPLACEMENT = "charset=UTF-8\"";

   private static final String LS = System.getProperty("line.separator");

   private RegexReplacer() {
      char fs = File.separatorChar;
      String root = new File("").getAbsolutePath();
      String testDataPath = String.format("%s%c%s", root, fs, "testdata");

      System.out.println("TestDataFilePath: " + testDataPath);

      File inFile = new File(testDataPath);

      scanAndReplacePattern(inFile);
   }

   /**
    * Detect charset and convert content of this file. Note that, this is approximate action, not 100% reliable.
    */
   private void scanAndReplacePattern(File file) {
      if (file.isFile()) {
         performReplace(file, REGEX, REPLACEMENT);
      }
      else if (file.isDirectory()) {
         File[] fs = file.listFiles();

         if (fs != null) {
            for (File f : fs) {
               scanAndReplacePattern(f);
            }
         }
      }
   }

   private void performReplace(File file, String regex, String replacement) {
      try {
         BufferedReader br = new BufferedReader(new FileReader(file));
         String line;
         StringBuilder sb = new StringBuilder();

         while ((line = br.readLine()) != null) {
            line = replaceInLine(line, regex, replacement);
            sb.append(line).append(LS);
         }

         br.close();

         BufferedWriter bw = new BufferedWriter(new FileWriter(file));
         bw.write(sb.toString());
         bw.close();
      }
      catch (Exception e) {
         e.printStackTrace();
      }
   }

   private String replaceInLine(String line, String regex, String replacement) {
      String result = line.replaceAll(regex, replacement);

      if (!result.equals(line)) {
         System.out.println(String.format("Before: %s", line));
         System.out.println(String.format("After:  %s", result));
         System.out.println();
      }

      return result;
   }

   public static void main(String[] args) {
      new RegexReplacer();
   }
}
